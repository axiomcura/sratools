import os 
import sys
import argparse 
import textwrap
import pandas as pd 
import re 
import shutil
from collections import Counter, defaultdict


def dmnd_parser(dmnd_files) -> list:
    """ Parses dmnd files and extracts all the hits"""

    file_data = defaultdict(list)  # NOTE: contains {name, rawdata}
    for dmnd_file in dmnd_files:
        with open(dmnd_file, 'r') as d_file: 
            name = os.path.splitext(dmnd_file)[0]
            for dmnd_data in d_file: 
                data = dmnd_data.split() 
                file_data[name].append(data) 
    return file_data 


def get_gene_dataframe(labled_raw_data) -> pd.DataFrame:
    """ This function blocks the data based on the """

    # Iterate through the list 
    dmnd_data = defaultdict(list)  # NOTE: contains {name: [unique_id, gene_hits, e_val]}
    for name, raw_data in labled_raw_data.items():
        for line_data in raw_data:
            unique_id = [line_data[0].split(':')[-1]]
            raw_gene_hits = line_data[1].split('|')[1:]
            e_val = [line_data[10]]
        
            # removing any nongenes in the gene_hits 
            r = re.compile('[^\d\.]') # creating pattern search 
            gene_hits = [' '.join(list(filter(r.match, raw_gene_hits)))] 
            new_data = unique_id + gene_hits + e_val
            dmnd_data[name].append(new_data)

    dmnd_columns = [
        "unique_id",
        "gene_hits",
        "e_val"
    ]

    labled_grouped_df = defaultdict(list) # Note: contains {name: groupdf} groupdf is a tuple
    for name, e_data in dmnd_data.items():
        raw_dmnd_df = pd.DataFrame(e_data, columns=dmnd_columns)
        raw_dmnd_df['e_val'] = raw_dmnd_df["e_val"].astype(float)
        grouped_dmnd_df = raw_dmnd_df.groupby(by=["unique_id"])
        labled_grouped_df[name].append(grouped_dmnd_df)

    gene_df = defaultdict(list)
    for name, grouped_set_df in labled_grouped_df.items():
        for grouped_df in grouped_set_df:
            for _, df in grouped_df:
                lowest_val = df.loc[df['e_val'] == df['e_val'].min()].values.tolist()
                selected_genes = list(set(tuple(gene_data) for gene_data in lowest_val))
                gene_df[name].append(selected_genes[0]) # [0] was applied in order to break the tuple (tuple opening)

    labled_gene_df = defaultdict(list)
    for name, genes in gene_df.items():
        gene_df = pd.DataFrame(genes, columns=dmnd_columns)  
        labled_gene_df[name].append(gene_df.to_dict())
    
    return labled_gene_df #TODO: issue! the pandas dataframe is lost when appended to the default dict 


def get_row_index(labled_genedf):
    """ gets all the names of the files and uses it as column index. """
    row_index = []
    for name, gene_df in labled_genedf.items():
        print(pd.DataFrame(gene_df))

        # for raw_gene in gene_hits: 
        #     # gene = raw_gene.split()
        #     print(raw_gene)


def gene_counter(gene_df):
    """ counts the number genes present in each file. """
    # if isinstance(gene_df, list): 
    #     for df in gene_df:
    #         gene_list = df.loc["gene_hits"].values.tolist()
            
    pass


def construct_matrix():
    """ Obtains all of the data from gene_counter() and converts it to a single column. """
    pass


def help_message():
    """ Displays help message"""
    
    print(textwrap.dedent("""
    This program parses the output files obitained from Diamond and creates a column of
    gene his. These coulmns serve as the building blocks in order to constuct a gene count
    matrix. 

    [ Required Arguments ]


    [ Optional Arguments ] 

                          """))
    exit()
    pass

        
if __name__ in '__main__':
    
    # checking for depeendices 
    if shutil.which('diamond') is None:
        print('Error: Diamond program is not found')
        exit()
        
    # help message 
    if len(sys.argv) == 1: 
        help_message()

    elif sys.argv[1] == '-h' or sys.argv[1] == '--help':
        help_message()
    


    # CLI
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-i', '--input', type=str, nargs="+", help='List of dmnd_files')
    # parser.add_argument('-k', '--count', type=int, help="maximum count of querey match")
    args = parser.parse_args()

    parsed_data = dmnd_parser(args.input)
    labled_gene_df = get_gene_dataframe(parsed_data)
    get_row_index(labled_gene_df)
    # counted_genes = gene_counter()
    # parsing obtaining columns names 
