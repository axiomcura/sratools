# the sing core handler sra_analysis works then we can do a multi core version 
import os
import sys 
import multiprocessing 
import sra_analysis as sra 
from multiprocessing import Pool
import argparse 
def sra_analysis(sra_id, outfile=True, ):
    
    # invote function 
    sra.sra_aln() # reutns a list of passed or failed 

def core_handler(sra_id, cores=8):
    """
    [Summary]
    This function controls the distrubution of data in each core. 
    
    [Details]
    The core_handler function will spawn muiltple cores to run these processes. Each core will 
    be assigned a sub group of data and will execute the core_handler(). This will result in 
    all the cores to run in parallel.  

    [requires]: 
    - list of all SRR data 

    """
    pass

if __name__ in '__main__':

    if sys.argv == 1:
        print('enter help message here')

    # CLI inputs 
    parser = argparse.ArgumentParser(description="Multi-core version os sra_aln.py")
    
    # grouping argueuments 
    required = parser.add_argument_group('required')
    optional = parser.add_argument_group('optional')

    # arguments for this program 
    required.add_argument()

    optional.add_argument('-p', '--proc', nargs='?', type=int, default=5, 
                          help='number of processors to handle the analysis') 
    optional.add_argument('-c', '--columns', type=str, nargs='?', default='Run download_path', 
                          help='addition of other columns')

    
    # Parse meta data 
    
    
    
    # user can ask what other parts of this meta data can be written in the outfile 
    # the requests colums will later inputed into the sra_analysis function