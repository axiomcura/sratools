#!/home/erikserrano/Programs/anaconda3/bin/python
import subprocess 
import os, sys 
import argparse
import re
import shutil
import subprocess
import textwrap 
 
 # TODO: replace the log file with the imported log file. 
 # TODO: add -w options to control amount of workers per alignment (currently not in main code)
 # TODO: Prevent fasterq reinitiation if reusing the same SRA filw
 # TODO: add -m (maybe this might be scaled internally) use 50% amount of ram 
def create_bowtie_indx(raw_seq, idx_name):
    """ Creates a bowtie index for bowtie alignments 
    
    paramters
    ---------
    raw_seq : str
        Path that directly points to a '.fasta' file
        
    idx_name : str 
        Selected name for the index 
        
    returns
    -------
    Path to the created index
    """
    
    #checking the the fasta file exists    
    if not os.path.exists(raw_seq):
        print("Fasta file is not found")

    # checks if the directory exists, if so, the creation block is skipped
    indx_dir = '{}_indx'.format(idx_name)
    if not os.path.exists(indx_dir):
        print('creating index file\n') 
        os.mkdir(indx_dir)
        subprocess.run(['bowtie2-build', '{}'.format(raw_seq),
                         './{}/{}'.format(indx_dir, idx_name)])

    return os.path.abspath(indx_dir)


def sra_aln(sra_file, f_name, sequence, aln_type='local', sensitivity='sensitive', outname='SRA_outfile.csv'): # add indx_path 
    
    """ Function that uses 
    
    Paramters 
    ---------
    sra_file : str
        Collection of the SRA_data directories that is going to be used for analysis. 
    
    f_name : str 
        naming scheme that is going to be used for outputed files. 
    
    sequence : str 
        sequences that is going to be used for creating the bowtie index.
    
    aln_type : str 
        The type of alignment that will be conducted in the analysis. Default
        is set to 'local'. If set to 'end-to-end', then it will conduct a global
        alignment
        
    preset : str
        Uses Bowtie's variables to select the stringency of the alignment. Since by default
        the aln_type is set to 'local', then the preset setting is defaulted to 'sensative-local'
    
    outname : str
        Name of the output file where the results are being written to. 
        

    Returns
    -------
        Each SRA_data directory will have 'sorted.bam' file and an outfile indicating
        if passed or failed test. 
        
    """

    # prepairing files 
    cwd = os.getcwd()
    outfile = open('./{}'.format(outname), 'a')

    # creating bowtie alignment 
    indexPath = create_bowtie_indx(sequence, f_name)

    status = []
    # temportrary because it is not writing files 
    for srr_dir in sra_file:

        # getting SRR ids from the directory name
        cwd = os.getcwd()
        path_to_srr = os.path.abspath(srr_dir)
        os.chdir(path_to_srr) 
        ids = srr_dir.replace('_data', '')
        
        # initiating fasterq dump
        # NOTE: skips This step if .fastq file exists
        if not os.path.exists('./{}.fastq'.format(ids)):
            try: 
                print('Initiating Fasterq-dump')
                subprocess.run(['fasterq-dump', './{}'.format(ids), '--concatenate-reads'], 
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE )
            except subprocess.CalledProcessError: 
                print('Error has been captured, Moving {} to failed folder'.format(path_to_srr),
                    'please check your files')
                
                os.chdir(cwd)
                
                if not os.path.exists('./broken'):
                    os.mkdir('broken')

                shutil.move(path_to_srr, 'broken')
                continue
        else: 
            print('{}.fastq already exists, skipping this step'.format(ids))
       
       # Bowtie alignment 
        print('Running Bowtie2 alignment')
        # if using the end-to-end aligment
        if aln_type == 'end-to-end':
            print('Alignment type: end to end. Set at: {}'.format(sensitivity))
            params = ["bowtie2", "-q", "-p", "6", "-x", "{}/{}".format(indexPath, f_name), "-U", 
                    "{}.fastq".format(ids), "--quiet", "--al-gz", "./{}_{}.fastq.gz".format(ids[-3:], f_name), 
                    "--{}".format(aln_type), "--{}".format(sensitivity), "-S", "{}_{}.sam".format(ids[-3:], f_name)]
            subprocess.run(params, stdout=subprocess.PIPE)

        # if using local aligment 
        else:
            print('Alignment type: local. Set at: {}'.format(sensitivity))
            params = ["bowtie2", "-q", "-p", "6", "-x", "{}/{}".format(indexPath, f_name), "-U", 
                    "{}.fastq".format(ids), "--quiet", "--al-gz", "./{}_{}.fastq.gz".format(ids[-3:], f_name), 
                    "--{}".format(aln_type), "--{}-{}".format(sensitivity, aln_type), "-S", "{}_{}.sam".format(ids[-3:], f_name)]
            subprocess.run(params, stdout=subprocess.PIPE)
       
        # running samtools view 
        try:
            print('viewing sam files')
            sam_view_cmd = ["samtools", "view", "-@", "4", "-S", "-bu", "{}_{}.sam".format(ids[-3:], f_name),
            "-o", "./{}_{}.bam".format(ids[-3:], f_name)]
            subprocess.run(sam_view_cmd, stdout=subprocess.PIPE, check=True)
        except subprocess.CalledProcessError:
            print('Error has been captured, Moving {} to failed folder'.format(path_to_srr),
                  'please check your files')
            
            os.chdir(cwd)
            
            if not os.path.exists('./broken'):
                os.mkdir('broken')

            shutil.move(path_to_srr, 'broken')
            continue
            
        # running samtools sort  
        print('sorting sam files')
        subprocess.run(["samtools", "sort", "-@", "4", "-T", "Amuc_temp",         
                       "{}_{}.bam".format(ids[-3:], f_name), "-o", "{}_{}.sorted.bam".format(ids[-3:], f_name)],
                       stdout=subprocess.PIPE)

        # checking overall mapped regions 
        proc_mappings = subprocess.run(["samtools", "view", "-@", "4", "-c", "-F", "260", 
                                        "{}_{}.sorted.bam".format(ids[-3:], f_name)], stdout=subprocess.PIPE)
        mapping_results = proc_mappings.stdout.decode('utf-8').strip('\n')
        print('Number of mapped regions {}'.format(mapping_results))

        if int(mapping_results)!= 0: 
            print('{},{},passed'.format(ids,mapping_results))
            status.append('{},{},passed\n'.format(ids,mapping_results))
        else: 
            print('{},{},failed'.format(ids,mapping_results))
            status.append('{},{},failed\n'.format(ids, mapping_results))
            
        #removing files 
        print('removing files')
        rm_cmd = ["rm  -rf {0}_{1}.bam {0}_{1}.sam" .format(ids[-3:], f_name)]
        subprocess.run(rm_cmd, stdout=subprocess.PIPE, shell=True)
        os.chdir(cwd)
    
    # writting the outfile data 
    for stat in status:
        outfile.write(stat)
            
    outfile.close() 
    # failed.close()

def help_message():
    print(textwrap.dedent("""
    [ Summary ]
    sra_analysis.py is a script thatwraps around all the necessary 
    steps when searching for the a specific gene in unpaired meta-genomics reads. 
    
    
    [ Required Arguments ] 
    -i, --input		    All the sra files in a folder ending in ‘_data’
    -f, --fasta		    The target sequence(s) used for alignment
    -x, --indexName 	The index name used when creating a bowtie index


    [ Optional Arguments ]
    -a, --alignment 	The type of alignment conducted [default: local]
    -s, --stringency 	The stringency of the alignment search [default: sensitive-local]
                        [choices: very-fast, fast, sensitive, very-sensitive]
    -m , --memory		The amount of memory used in each process [default: 2]
    -w, --workers		The amount of workers used when conducting the analysis [default: 4]
                            """))
    exit()
         
if __name__ in '__main__':
    try:
        parser = argparse.ArgumentParser(description='automationg of bowtie') 
        required = parser.add_argument_group('Required options')
        optional = parser.add_argument_group('Optional Arguments')
        
        #required arguments 
        required.add_argument('-i', '--input', nargs='+', help='input ids')
        required.add_argument('-f', '--fasta', type=str, help='fasta file foor indx')
        required.add_argument('-x', '--indexName', type=str, 
                              help='indx name for aligment')
        
        #optional arguments 
        optional.add_argument('-a', '--alignment', choices=["end-to-end", "local"],type=str, default='local',
                              help='selecting the type of alignments local or end-to-end')
        optional.add_argument('-s','--stringency', choices=["very-fast", "fast", "sensitive", "very-sensitive"] ,type=str,
                              default='sensitive', help="Sensativity of the alignment")    
        optional.add_argument('-o', '--outfile', type=str, default='SRA_outfile.csv',
                              help='Name of the output file.')
        
        args = parser.parse_args() 
    except:
        help_message()
        
    #creating bowtie indx
    sra_aln(args.input, args.indexName, args.fasta, args.alignment, args.stringency, args.outfile)
    
