#!/home/exec/anaconda3/bin/python
import urllib.request
import argparse 
import os
import pandas as pd 
import shutil
import subprocess 
#import threading # will soon use for 3 threads per downloads 



def get_sra_data(accession, outdir):
    """Summary: 
        Tool of obtaining all SRA files in a bioproject ID (accession that starts with PRJ) or
        indiviual SRA files (accession that starts with SRR). 
    
    Arguments:
        accession {[str]} -- Unique id that pertains to SRA or BioPorject data file 
        
        src {[str]} -- source where the input id is from. if SRR then then the -s 'SRA' 
        if it starts with 'PJR' then -s 'BIO'
    
    Raises:
        ValueError: will be invokved if the provided accession is not found or invalid to the respected 
        database. 
    """

    # bioproject accession is recognize 
    if accession[0][:3].lower() == 'prj':
        meta_name = 'BioProj_{}_metadata.csv'.format(outdir)
        try: # downloading metafile accoessiated with PJR meta 
            print('downloading {} meta data'.format(accession[0]))
            url_bio = 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term={}'.format(accession[0])
            urllib.request.urlretrieve(url_bio, meta_name)
        except ValueError:
            print('Acession number was not correct, or could not establish connection to the NCBI server')
            exit()

        # parsing metadata and obtain SRA ID and link 
        # meta_DF = pd.read_csv(meta_name)
        meta_DF = pd.read_csv(meta_name)
        # check if path to download exists if not use the SRA ids 
        data_dir ='BioProject_{}-data'.format(outdir)
        if 'download_path' in meta_DF.columns:
            dl_links = dict(zip(meta_DF['Run'], meta_DF['download_path']))
            os.mkdir(data_dir)
            os.chdir(data_dir)
            
            # downloading all SRAs and putting it into their own folders 
            for SRAid, link in dl_links.items():
                sradata_dir = '{}_data'.format(SRAid)
                
                if os.path.exists(sradata_dir):
                    print('{} is already downloaded'.format(SRAid))
                    continue 
                
                print('downloading sra {}'.format(SRAid))
                wget_cmd = 'wget {} -O {}'.format(link, SRAid)
                subprocess.run(wget_cmd, shell=True)
                os.mkdir(sradata_dir)
                shutil.move(SRAid, sradata_dir)

        else:
            # this will not work, link is not general 
            dl_links = list(meta_DF[['Run']])
            for sra_id in dl_links:
                print('MESSAGE: Downloading sra {}'.format(sra_id))
                urllib.request.urlretrieve('https://sra-download.ncbi.nlm.nih.gov/traces/sra78/SRR/008462/{}'.format(sra_id), sra_id)
                print('MESSAGE: {} download complete!'.format(sra_id))
    
    # recognizing if its a SRA accession
    #NOTE: not a general solution 
    #NOTE: link is not generalized to all SRA ids!!!!!  
    elif accession[0][:3].lower() == 'srr':
        try:
            for sra_id in accession:
                print(sra_id)
                sra_dir = '{}_data'.format(sra_id)
                
                print('downloading sra {}'.format(sra_id))
                url_sra = "wget https://sra-download.ncbi.nlm.nih.gov/traces/sra78/SRR/008462/{}".format(sra_id)
                os.system(url_sra)
                print('{} download compelte!'.format(sra_id))
                os.mkdir(sra_dir)
                shutil.move(sra_id, sra_dir)
        
        except ValueError: 
            print('The accession number you have provided is inccorect for the selected source.')
            exit()

    else:
        raise ValueError('The accession number provided is invalid. Bioproject ID starts with PJR and SRA ids start with SRR')

if __name__ in '__main__':

    # cli commands start here 
    parser = argparse.ArgumentParser(description="CLI commands for downloading SRAfiles")
    parser.add_argument('-i', '--input', type=str, nargs="+",
                        help='enter accenssion number ')
    parser.add_argument('-f', '--filename', type=str, required=True, 
                        help='Name of the file created containing all the data')
    args = parser.parse_args()
    get_sra_data(args.input, args.filename)
