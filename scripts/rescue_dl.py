import pandas as pd 
import os 
import sys 
import argparse
import subprocess
import shutil

def save_downloads(metadata):
    # dataframe 
    meta_DF = pd.read_csv(metadata)
    
    # parse the metda data file
    if 'download_path' in meta_DF.columns:
        dl_links = dict(zip(meta_DF['Run'], meta_DF['download_path']))
        
    # downloading all SRAs and putting it into their own folders 
    for SRAid, link in dl_links.items():
        sradata_dir = '{}_data'.format(SRAid)
        
        if os.path.exists(sradata_dir):
            print('{} is already downloaded'.format(SRAid))
            continue 
        
        print('downloading sra {}'.format(SRAid))
        wget_cmd = 'wget {} -O {}'.format(link, SRAid)
        subprocess.run(wget_cmd, shell=True)
        os.mkdir(sradata_dir)
        shutil.move(SRAid, sradata_dir)

if __name__ in '__main__':
    
    parser = argparse.ArgumentParser(description='#')
    parser.add_argument('-i','--input', type=str, required=True, 
                        help="Enter metadata.csv file")
    args = parser.parse_args()
    
    # executing rescue 
    save_downloads(args.input)