import glob
import pandas as pd
import os
import sys
import argparse
import numpy as np
import re
from collections import Counter

#########################################################
# geneMatrix.py: program for creating a gene matrix count
# created by Erik Serrano
# Flores' Lab
# email erik.serrano.318@my.csun.edu
# stored into sraTool package in gitlabs, request only
#########################################################


def data_parser(input_lables):
    """
    Summary
    -------
    Reads the files and prettifyes it by making it readable fort he script

    arguments
    ---------
    input file {dictionary} -- files that is going to be read and its contents inside will be 
    prettifyed 

    returns 
    -------
    a dictionary that contains {"strain_name" : "prettifyed contents"}
    """
    prettied_data = {}  # {'strain_name' : []}
    for strain_name, file_path in input_lables.items():
        file_contents = []
        print(strain_name, file_path)
        with open(file_path, 'r') as contents:
            for lines in contents:
                strip_lines = lines.strip('\n')
                split_conts = strip_lines.split('\t')
                file_contents.append(split_conts)
        prettied_data[strain_name] = file_contents

    return prettied_data


def data_labler(input_file):
    # what if a directory is provided? (only takes one directory)
    for dir_path in input_file:
        directory = os.path.isdir(dir_path)
        relative_path = os.path.relpath(dir_path)
        if directory:
            strain_name = {}  # {'strain_name':'path/to/filename.txt'}
            data_files = glob.glob('{}/*.txt'.format(relative_path))
            for f_path in data_files:
                f_name = f_path.split('/')[-1]  # last index is always the filename
                s_name = f_name.split('_')[0]
                strain_name[s_name] = f_path
        else:
            # get gene name from file name
            strain_name = {}  # {'strain_name':'fpath/to/filename.txt'}
            for f_path in input_file:
                s_name = f_path.split('_')[0]
                strain_name[s_name] = f_path

    return strain_name

def create_column_data(datafiles, toolhits=1):
    """
    creating a data frame that removes any rows containing only 1 tool hit 
    and also seperating multiple genes 

    arguments 
    ---------
    datafiles {dict} : key value pairs that associate with 'strain_name' : 'file_path' 
    toolhits (int)   : Accepting only rows that have more than 2 hits 

    returns
    -------
    Returns a dictioanry that contains the strain name paired with the list of
    genes that were recorded in the initial data. The list of genes is sorted
    example output --> {'Strain_name': ['gene1','gene2','gene3','gene4']}
    """
    
    col_data = {}
    for strain_name, datalist in datafiles.items():

        # creating DataFrame
        dataframe = pd.DataFrame(datalist[1::], columns=datalist[0])
        raw_data = dataframe[["HMMER", "DIAMOND", "#ofTools"]]
        filtered_data = raw_data[dataframe["#ofTools"].astype(int) > toolhits]

        #converting data into a list of tuples (hmmer_hit, diamond_hit)
        hmmer_data = list(filtered_data["HMMER"])
        diamond_data = list(filtered_data["DIAMOND"])
        data_pairs = tuple(zip(hmmer_data, diamond_data))
        # print(data_pairs)

        # iterating through gene and filtering via regex 
        gene_list = []
        for hmmer_hit, diamond_hit in data_pairs: 
            if '+' in hmmer_hit:
                two_hits = hmmer_hit.split('+')
                for genehits in two_hits: 
                    gene_hit = re.sub(r'\([^)]*\)', '', genehits)
                    gene_list.append(gene_hit)
            elif 'N' in hmmer_hit:
                gene_hit = re.sub(r'\([^)]*\)', '', diamond_hit) 
                gene_list.append(gene_hit)
            else:
                gene_hit = re.sub(r'\([^)]*\)', '', hmmer_hit)
                gene_list.append(gene_hit)
        col_data[strain_name] = sorted(gene_list)
        
        
    return col_data


def create_indx_row(col_data, sort=True):
    """
    summary
    -------
    Finds the list containg the longest amount of gene hits and converts it 
    to the primary index column for the matrix.
    
    arguments
    ---------
    col_data {dict} - a dictionary containg the strain name and the list of genes that were 
    found. 

    sort [bool] -  default set to 'True' results in a sorted index list  
    
    returns
    -------
    Returns a list that would be used as the index column for the matrix
    """
    all_strain_hits = [] 
    for gene_hit_list in col_data.values():
        all_strain_hits.append(set(gene_hit_list))
    
    # returning the logest list and is the longest column index 
    index_column = sorted(list(max(all_strain_hits, key=len)))
    return index_column

def create_indx_column(labled_data):
    """
    summary
    -------
    Creates an column index for the data frame 
    
    Arguments:
    labled_data {[dict]} -- diictionary containing the strain names 
    
    Returns:
    List - containing all the strain names. 
    """
    column_indx = [strain_name for strain_name in labled_data.keys()]
    return column_indx
    
def gene_counter(col_data):
    """
    summary
    -------
    Counts how many gene hits that the strain has. Then converts it into a dictioanry 
    that will be later used as the data for building the geneMatrix
    
    Arguments:
    col_data {[dict]} -- a dictionry that contains key value pair of 'strain_name' : 
    [list of genes ]
    
    returns 
    -------
    returns a dictionary that contains the strain name and gene count values 
    """

    matrix_col_data = {}
    for strain_name, gene_list in col_data.items():
        counted_genes = Counter(gene_list)
        matrix_col_data[strain_name] = dict(counted_genes)

    return matrix_col_data 

def create_gene_matrix(row_index, column_index, data_columm, outfile="out_geneMatrix", out_type="csv", update=False):
    """ 
    Summary
    -------
    Utlize the created column data and forms it to a matrix 
    Arguments
    ---------
    row_column {[list]} - List that will represent the index of each row 
    column_index {[list]} - List that will represent the index of each column
    data_columms {[dictioanry]} - The raw data the cotained the strain name and the counted 
    genes 
    
    Keyword Arguments:
    ------------------
    outfile {str} -  name of the output file being written (default: "geneMatrix")
    out_type {str} -  the format that is going to be (default: 'csv')
    update {bool} - Update the contents if the csv exists in the current directory
    """
    #intializing empy dateframe
    empty_gene_matrix = pd.DataFrame(index=row_index, columns=column_index).replace('NaN', 0)

    # unacking extracted data and converting it into data frames
    for strain_name, col_dat in data_columm.items():
        strain_data = pd.DataFrame(col_dat.values(), index=col_dat.keys(), columns=[strain_name])
        empty_gene_matrix.update(strain_data)
    
    gene_matrix = empty_gene_matrix.fillna(0).astype(int)
    
    if out_type.lower() == "csv":
        #updateing data to existing file 
        if os.path.exists('{}.csv'.format(outfile)) and update == True:
            print(update)
            print('MESSAGE: Updating {}.csv'.format(outfile))
            gene_matrix.to_csv('{}.csv'.format(outfile))
            
        # program error if the file exists  
        elif os.path.exists('{}.csv'.format(outfile)):
            raise FileExistsError("The file {}.csv already exists, please use -u to over write and update with new data".format(outfile))
        
        else:
            print("MESSAGE: Format was set to '{}'".format(out_type))
            gene_matrix.to_csv('{}.csv'.format(outfile))
    else:
        raise ValueError("Could not recognize requested format: {}".format(out_type))
        

def program_help():
    """
    Displays options and help message if an error occurs in the arugments 
    """
    #TODO: Currently writting a docs
    print('Help option is invoked')


if __name__ in '__main__':

   # CLI argiments
    # try:
    parser = argparse.ArgumentParser(description="geneMatrix Count file ")
    required = parser.add_argument_group("Required Arguments")
    optional = parser.add_argument_group("Optional Arguments")

    # required arguments 
    required.add_argument('-i', '--input', type=str, nargs="+", required=True,
                            help="List of txt files for parsing")
    #optional arguments
    optional.add_argument('-r', '--raw', type=str, required=False,
                            help="Saving raw data to current directory")
    optional.add_argument('-o', '--outfile', type=str, required=False, default="out_geneMatrix",
                            help="name of the output file created")
    optional.add_argument('-f', '--format', type=str, required=False, default="csv",
                            help="Format of the output file")
    optional.add_argument('-u', '--update', action="store_true", default=False, required=False,
                          help="updates csv if the file exists")
    args = parser.parse_args()
    # except:
        # print('help option is here')
        # program_help()
        
    print(args.update)

    # parsing the files
    print('MESSAGE: Loading data from: {}\n'.format(args.input))
    labled_names = data_labler(args.input)
    raw_data = data_parser(labled_names)
    
    # now process the raw data and try to make a single columns 
    print("MESSAGE: Extracting Data\n")
    all_col_data = create_column_data(raw_data)
    
    print("MESSAGE: Constructing gene matrix\n")
    # components for constructing the matrix 
    indx_row = create_indx_row(all_col_data)
    indx_col = create_indx_column(raw_data)
    matrix_data = gene_counter(all_col_data)
    create_gene_matrix(indx_row, indx_col, matrix_data, outfile=args.outfile, out_type=args.format, update=args.update)
    print('COMPLETED: Gene Matrix saved as {}.{}'.format(args.outfile, args.format))