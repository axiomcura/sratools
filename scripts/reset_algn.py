#!/home/erikserrano/Programs/anaconda3/bin/python
import os 
import argparse 
import glob

def reset_aln_files(data_files, all_files):
    for data_file in data_files:
        print('Reseting {}...'.format(data_file))
        files = glob.glob('{}/*.gz'.format(data_file)) + glob.glob('{}/*.bam'.format(data_file)) + glob.glob('{}/*.sam'.format(data_file)) 
        if len(files) == 0:
            print('{} is skipped. Already reseted'.format(data_file)) 
        elif all_files is True: 
            all_files = files + glob.glob("{}/*.fastq".format(data_file))
            for file in all_files: 
                os.remove('{}'.format(file))
        else:    
            for file in files:
                os.remove('{}'.format(file))

if __name__ in '__main__':
    parser = argparse.ArgumentParser(description='remove all files from previous analysis')
    parser.add_argument('-i', '--input', nargs='+', type=str, required=True,
                        help='list of the data folder used in sra_analysis')
    parser.add_argument('--all', type=str, required=False, default=False)
    args= parser.parse_args()
    
    reset_aln_files(args.input, args.all)
    print('reset complete!')
