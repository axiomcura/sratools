import sys 
import argparse 
import multiprocessing as mp
import textwrap
import subprocess


## TO DO LIST ##
# TODO: enter error handling  

def run_dmnd_blastx(seqs, db_path, n_threads):
    """ Execute diamond command and starts mapping genes"""
     
    # running diamond 
    for seq_file in seqs:
        name = seq_file.replace('.fasta','')
        dmnd_cmd = 'diamond blastx --db {} -q {} -o {}.matches..out -k 3 --threads {} -f 6 -e 1.0e-06 '.format(db_path, 
                seq_file, name,  n_threads).split()
        subprocess.run(dmnd_cmd) #stdiout=PIPE, stderr=STDOUT)

    #NOTE: enter error handling here 

def mp_handler(seq_files, lead_cores):
    
    with mp.Pool(lead_cores) as pool: 
        pool.map(run_dmnd, seq_files)
        pool.join()
        pass
    
def help_message():
    print(textwrap.dedent("""
    [ Summary ] 

    rundmnd.py is a program that spawns multi-processing units
    where each process holds a querey and executes diamond. 
    This program focuses on using as much resources as possible
    in order to conduct these gene searches. 

    [Required arguments] 
    
    -i, --input         All of the nucleotide sequence files 
    
    -d, --database      A path that directs to a specific 
                        database. 


    [Optional arguments]
    
    -l, --leadcores     Number of processors that will handle 
                        and execute diamond 

    -t, --threads       Number of threads per processors used 
                        when executing and calculating in 
                        diamond

            """))

for __name__ in '__main__': 
    
    #help arguments 
    if len(sys.argv) == 1:
        help_message()
    
    elif sys.argv[1] == '-h' or sys.argv[1] == '--help':
        help_message()
    
    #CLI arguments 
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')    
    optional = parser.add_argument_group('optional arguments')
    required.add_argument('-i', '--input', type=str, nargs='+', required=True)
    required.add_argument('-d', '--database', type=str, required=True)
    #optional.add_argument('-l', '--leadcores', required=False, type=str, defaut=2)
    optional.add_argument('-t', '--threads', type=int, required=False, default=4)
    args = parser.parse_args()


    run_dmnd_blastx(args.input, args.database, args.threads)


    # executing the data to the mulitprocess handler 
    #mp_handler(args.input, args.lead_cores)
