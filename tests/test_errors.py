import unittest

from sratools.debugg.errors import MatrixBuildingError

class TestUnitTest(unittest.TestCase):
    
    def test_matrix_error(self):
        with self.assertRaises(MatrixBuildingError):
            raise MatrixBuildingError("Error message")
        
        
if __name__ in '__main__':
    
    unittest.main()
    