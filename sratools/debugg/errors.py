################################################################################
# errors.py 
# User defined errors for better debuggin 
# This packages will be used for all the other scripts in error instances 
# 
# Author: Erik Serrano 
# Email: erik.serrano.318@my.csun.edu
################################################################################

class DmndInputError(Exception):
    """ Refers either to the command or input files used when executing Diomond """
    
class MatrixBuildingError(Exception):
    """ Error in building the matrix """