##########
# NOTES
##########

# Argparse 
- seems like there is an issue with error handling with arpgarse, It does not through an exception. 
    - If the user incorrectly inputs the argument, then the help() function should be invoked via try-except 
    ```python
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--input',required=True, help="help message")
    except: 
        help_function() 
    ```
    - how ever, it cannot capture the error (look for a way )
