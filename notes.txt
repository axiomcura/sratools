subprocess module is extremely buggy when conducting this automation tests. Eventhough its bad practice. I resorted to the os modules by using the os.system.

The problem is that the commands uses for bowtie2 and samtools is that is uses shell termonology like ">". eventhough it is a part of their commands, python's subprocess module does not know that ">" is a bowtie2 command and not shell. 

This error caused me to resort to os.system which is a subshell system that allows to use shell commands when conducting automations. however, its is less flexible and will not be able to capture errors, which makes it difficult to debug 

for same tools, instead to using > use -o 